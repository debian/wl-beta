Source: wl-beta
Section: lisp
Priority: optional
Maintainer: Tatsuya Kinoshita <tats@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: texinfo
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/wl-beta
Vcs-Git: https://salsa.debian.org/debian/wl-beta.git
Homepage: https://github.com/wanderlust/wanderlust
Rules-Requires-Root: no

Package: wl-beta
Architecture: all
Depends: emacsen-common, semi, flim, apel, ${misc:Depends}
Recommends: emacs-nox | emacs, ca-certificates
Suggests: gnupg, gnutls-bin, openssl, starttls, x-face-el, w3m-el, bbdb, im, namazu2, maildir-utils, notmuch, bogofilter | bsfilter | spamassassin, mu-cite, mhc
Replaces: wl, wanderlust2
Conflicts: wl, wanderlust2, xbase (<< 3.3.2.3a-2)
Provides: wl, imap-client, mail-reader, news-reader
Description: mail/news reader supporting IMAP for emacsen (development version)
 Wanderlust is a mail/news management system on emacsen.  It supports
 IMAP4rev1 (RFC2060), NNTP, POP and local message files.
 .
 The main features of Wanderlust:
 .
  - Pure elisp implementation.
  - Supports IMAP4rev1, NNTP, POP (POP3/APOP), MH and Maildir format.
  - Unified access method to messages based on Mew-like Folder Specification.
  - Mew-like Key-bind and mark handling.
  - Manages unread messages.
  - Interactive thread display.
  - Folder Mode shows the list of subscribed folders.
  - Message Cache, Disconnected Operation.
  - MH-like FCC. (Fcc: %Backup and Fcc: $Backup is allowed).
  - MIME compliant (by SEMI).
  - Transmission of news and mail are unified by Message transmitting draft.
  - Graphical list of folders.
  - View a part of message without retrieving the whole message (IMAP4).
  - Server-side message look up (IMAP4). Multi-byte characters are allowed.
  - Virtual Folders.
  - Supports compressed folder using common archiving utilities.
  - Old articles in folders are automatically removed/archived (Expiration).
  - Automatic re-file.
  - Template function makes it convenient to send fixed form messages.
 .
 This package provides a development snapshot version of Wanderlust.
